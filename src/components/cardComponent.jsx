
import React from 'react'
import { Card, Text } from 'react-native-paper'
import { Icon } from 'react-native-elements'


/**
 * 
 * @param {title} title titulo de la card
 * @param {subtitle} subtitle subtitulo de la card
 * @param {content} content Contenido de la card
 * 
 * @returns 
 */
const CardComponent = ({ title, subtitle, content, icon }) => {
    return (
        <>
            <Card.Title title={title} subtitle={subtitle}  />
            <Card.Content>
                <Icon
                    size={100}
                    type='ionicon'
                    name={icon} />
                <Text>
                    {content}
                </Text>
            </Card.Content>
        </>
    )
}

export default CardComponent

const styles=[
    styleIcon={
        width:50,
        height:50

    }
];  