
import React from 'react'
import { View, Text } from 'react-native'

const mealList =[
    {
        name:'Arroz',
        description:'Arroz blanco para acompañar a tus comidas',
        ingredientes:["1 taza de arroz",
                        "2 tazas de agua",
                        "un cubito de caldo de pollo",
                        "un diente de ajo",
                        "un pedazo de cebolla"
                    ],
        preparacion:[   
                        "limpiar el arroz y cortar el ajo y cebolla en cubitos pequeños",
                        "freir el arroz con el ajo y cebolla",
                        "una vez frito el arroz, agregar el agua y caldo de pollo",
                        "esperar a que se consuma el agua a fuego bajito"
                    ],
        src:'https://lh3.googleusercontent.com/u/0/d/11q8EyCRxm0gV6fbT8IoWkfdhSm4qQoMv=w200-h190-p-k-nu-iv1'
    },
    {
        name:'Barbacoa',
        description:'Carne de pollo o res, en rica salsa',
        ingredientes:["5 chiles guajillo", 
            "2 jitomates",
            "Un puño de chile seco",
            "Carne de res o pollo",
            "Hojas de aguacatillo", 
            ],
        preparacion:["Hervir los jitomates, chile guajillo y chile seco",
            "Licuar los ingredientes hervidos con ajo, cebolla y un poquito de comino",
            "En olla de presión freír lo que licuamos, y se le agrega sal",
            "Agregamos la carne con las hojas de aguacatillo y se tapa" 
            ],
        src:''
    },
    {
        name:'Tortitas de atun',
        description:'Para acompañar en la cuaresma o disfrutar el atun de manera diferente',
        ingredientes:["Pan molido", 
            "Una clara de huevo", 
            "Una papa mediana",
            "Atún" 
            ],
        preparacion:[ "Hervir la papa", 
            "Aplastar la papa en forma de pure",
            "Mezclar los ingredientes y le echamos sal",
            "Hacemos las bolitas y las ponemos a freír"
            ],
        src:''
    },
    {
        name:'Chiles jalapeños con queso crema',
        description:'ideal para acompañar',
        ingredientes:[
                        "chiles jalapeños",
                        "queso crema",
                        "tocino"
                    ],
        preparacion:[   
                        "lavar y desvenar los chiles jalapeños",
                        "rellenar con queso crema",
                        "envolver los chiles con tocino",
                        "meter en el horno 200°C durante 30-45 minutos o en asador"
                    ],
        src:'https://lh3.googleusercontent.com/u/0/d/1zQKv1rfn48aEXDJ8XefWtyT3jkRFIbzb=w200-h190-p-k-nu-iv1'
    }
]

const Meal = () => {
    return (
        <View>
            <Text>hola Meal</Text> 
        </View>
    )
}

export default Meal