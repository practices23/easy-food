import { createStackNavigator } from '@react-navigation/stack';
import React from 'react'
import Appetizer from './appetizer';
import Desserts from './desserts';
import Dips from './dips';
import HomePage from './HomePage';
import Meal from './meal';
import Pastas from './pasta';

const Stack = createStackNavigator();

const MyStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Inicio" component={HomePage} />
            <Stack.Screen name="Aperitivos" component={Appetizer} />
            <Stack.Screen name="Comida" component={Meal} />
            <Stack.Screen name="Pastas" component={Pastas} />
            <Stack.Screen name="Salsa" component={Dips} />
            <Stack.Screen name="Postres" component={Desserts} />
        </Stack.Navigator>

    )
}

export default MyStack