
import React from 'react'
import { View, Text } from 'react-native'

const dipsList =[
    {
        name:'Salsa Verde',
        description:'Ideal para la carnita asada',
        ingredientes:["chiles jalapeños",
            "cebolla",
            "ajo",
            "Sal al gusto",
            ],
        preparacion:["Freír unos 6 chiles con cebolla y 2 ajos",
            "Licuar los ingredientes con el aceite y un poco de agua",
            "Agregar un sal al gusto"
            ],
        src:''
    },
]

const Dips = () => {
    return (
        <View>
            <Text>hola Dips</Text> 
        </View>
    )
}

export default Dips