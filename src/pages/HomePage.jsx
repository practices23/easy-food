
import React from 'react'
import { View, Text } from 'react-native'
import { Button, Card } from 'react-native-paper'
import CardComponent from '../components/cardComponent'

const foods = [
    {
        title:'comida',
        logo:''
    }
]


function HomePage({ navigation }) {
    
    return (
        <View>
            <View style={{flexDirection:'row'}}>
                <View style={styleView}>
                    <Card onPress={()=>  navigation.navigate('Aperitivos')}>
                        <CardComponent title="Aperitivos" icon='fast-food' />
                    </Card>
                </View>
                <View style={styleView}>
                    <Card onPress={()=> navigation.navigate('Comida')}>
                        <CardComponent title="Comida" icon='restaurant' />
                    </Card>
                </View>
            </View>
            <View style={{flexDirection:'row'}}>
                <View style={styleView}>
                    <Card onPress={()=> navigation.navigate('Pastas')}>
                        <CardComponent title="Pastas" icon='pizza' />
                    </Card>
                </View>
                <View style={styleView}>
                    <Card onPress={()=> navigation.navigate('Salsa')}>
                        <CardComponent title="Salsas" icon='flame' />
                    </Card>
                </View>
            </View>
            <View style={{flexDirection:'row'}}>
                <View style={styleView}>
                    <Card onPress={()=> navigation.navigate('Postres')}> 
                        <CardComponent title="Postres" icon='cafe' />
                    </Card>
                </View>
            </View>
        </View>
        
    )
}

export default HomePage


const styleView = {
    width:'50%',
    padding:5
}