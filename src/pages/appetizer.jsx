
import React from 'react'
import { View, Text } from 'react-native'

const appetizerList =[
    {
        name:'Empanadas',
        description:'Antojitos jarochos preparados de masa',
        ingredientes:["Masa de maíz", "carne molida", "jitomate","cebolla" ],
        preparacion: ["Preparar la carne molida a la mexicana (se puede agregar chile al gusto)",
            "amasar la masa de maíz",
            "realizar bolitas de maíz y aplanar con una prensa (puede ser con la mano)",
            "rellenar con el picadillo y cerrar a la mitad",
            "aplastar la orilla para que no se abra",
            "freír en aceite caliente"
            ],
        src:''
    },
    {
        name:'Tacos dorados',
        description:'Tortilla de maiz, con un toque especial',
        ingredientes:[],
        preparacion:[],
        src:''
    },
    {
        name:'Niño envuelto',
        description:'Preparado con pan blanco ideal para compartir',
        ingredientes:[],
        preparacion:[],
        src:'https://lh3.googleusercontent.com/fife/AMPSemd8AOCVUvPQ87DxBa5Ioc0zlVQM64KntlYtBas-RgTBzCTlL5gRTKLcHwnba4jVmK8_eKN_Ur_1XyqVCcfjzPKh_jb3LP_l-_D7MS4w2oeXu9smZBI-ba0Hp7SnFV6pJFi7Wgl2xHinBaOwuf804-kIVUlTMtCsPv7IYSbG64JEKDXKZq6jg1-kHzEqOT0GZtckGSK1f-muoWyvN5XnM_AA52c2oX_SlShF-rrirYCutyPl9bClglSno06xNb5XHE2Oi25NJSs1GogMJTfRepvf1yo_29CBHpVyt6dlDwJHzMmN2plbrFzTrkAelNFf7qQfB8syW8WXfMYj205zaQ6MTuIzTOMr-4ytoesvXfkhzFpZnu5Hha4QNzyJQSft4Ynzq2fzGo_5wCiKSwjwDk1X6mO-EL51sdyFh3lIPo3eW8Y2S-4BAzQHX0FbM4s1FqVE_7M_-ikSjh1TjYHr4sZhQPEg-KqVhLFsExBrgfetSVUIOjnqOsenYvgO8E5aWeVNVCS_VGESQyUAAEbl8D7_GCQDr01c351NL_eI1emkjoHWcPPYOMiIxQfadjpVUFmaDarSE4vgJqkiOFAIuQgYbHvq7Z-MkrchynJm4fGzHPq6df0QqRpoZvcfuVXX83irMbXaVbiC8_hhHGxnOsEaEy0UsRXuHUxK-XSFe0kd_7E8KvoZYN2PsH6RYh5Qx8GDmCI1uy7BbZfG9ldPAuxgzjxoCN4tHTLUAcJmIKyKPF4DNgzANl6kPQNha6T_6i7UJWLzU94_RX0IOg5IyHinSmKMwOJB9PcLfP0bbP7BWO7viwmMA54VBGiIoZ2NM8A5C8pj2er2F4gAS4gCOeQMCFvyn6BKnl-nXlRKx6EgWrL3Qg57OS_fV9kFPiN5FB9PlOzUmzSVvGfoVVukBk7VIrMD1QrIN9P2cCL6CFFVMdMjmUb4npa6wPkOeVp3lVD-PQVpUQ-z--tzjzTKyIyZE4UPTrFjKLRmg-I2tAgerLKdPqVDQM8_Vt1RqKPmMFJ3xb9-0oqkHCc-gLk4C7dFiuMW9kojQ_X2EOXytsbYhtBkvdwSIrZQD_BGId6LLyzI1El71AaeAcvypqKTxyF2u8GxOmKB_bMnHhbQVUfciQFeqbbPu9ggglb1B2wnqKcyF9ZuIS0hmqxfrY3J7MaE-HwnUwJeeamwRfE8G9LIAao-u72osApcA3CVblpYTX_ReDgSNhp7P62RA2WvsvnMfXnGQYR9uT5DP39l3USAvA4fzbWYQr-mWcnMbvizs09T1HnALZswAraYZG3apMuPRkMiIF_6Qyc1qM8muVKUS8ZshVYTI1s4jlA2OvqBC9MOuI0SygiWB5Lq0tomTsZE84WckCzJREkeszBLrWhQ3PIs04IBhIxAIKjuf3Pc-IuERhdVp5c_kyvW-UNcPULgeTQWVBxj8RzyFPki4A6ySgfpp6yxZOIEyf3p1CTX1d_KsAJRQD70dC7xajNIrQ0NMkts=w200-h190-p-k-nu'
    }
]

const Appetizer = () => {
    return (
        <View>
            <Text>hola appetizer</Text> 
        </View>
    )
}

export default Appetizer