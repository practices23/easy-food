
import React from 'react'
import { View, Text } from 'react-native'

const mealList =[
    {
        name:'Lasaña',
        description:'Ideal para comidas especiales',
        ingredientes:["Pasta de lasaña",
            "Salsa boloñesa",  
            "2 Calabacitas", 
            "2 Zanahoria", 
            "500 grs Carne molida", 
            "400 grs Queso manchego", 
            ],
        preparacion:["Freír a término medio la carne, con la zanahoria y calabaciras en cubitos",
            "Hacer capaz iniciando con la pasta, después los ingredientes",
            "Cocinar al horno a 200°C durante 20 minutos"
            ],
        src:'https://lh3.googleusercontent.com/u/0/d/1SdOo72f8TUaBWDdoOjLa_ws5iFdStVc1=w1231-h977-iv1'
    },
]

const Pastas = () => {
    return (
        <View>
            <Text>hola Pastas</Text> 
        </View>
    )
}

export default Pastas