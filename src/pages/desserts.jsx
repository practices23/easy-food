
import React from 'react'
import { View, Text } from 'react-native'

const dessertsList =[
    {
        name:'Flan Napolitano',
        description:'un postre para compartir',
        ingredientes:["4 huevos",
            "1 lata de leche clavel", 
            "1 lata de lechera",
            "Un chorro de vainilla", 
            "Medio queso crema", 
            ],
        preparacion:["En el molde, derretir la azúcar en el fuego y repartir",
            "Licuar los ingredientes",
            "Integrar los ingredientes en el molde",
            "En una olla de presión cocinar a baño maría durante 20 minutos"
            ],
        src:''
    },
    {
        name:'Pastel imposible',
        description:'Flan con chocolate',
        ingredientes:[],
        preparacion:[],
        src:'https://lh3.googleusercontent.com/u/0/d/14YOzlDpEEbxxr1YeKzf4Rj1Kb8MTLw4Y=w200-h190-p-k-nu-iv1'
    },
]

const Desserts = () => {
    return (
        <View>
            <Text>hola Desserts</Text> 
        </View>
    )
}

export default Desserts